* Try 3 for fixing popups on pre-O versions, tweak transparent and Material light themes.
* Fix 4 for popups on Android O MR1 (8.1). (Android P is still misbehaving.)
