cSploit is a free/libre and open source (GPLed) Android network analysis and penetration suite which aims to be the most complete and advanced professional toolkit for IT security experts/geeks to perform network security assessments on a mobile device.

See more at www.cSploit.org.
Features

    Map your local network
    Fingerprint hosts' operating systems and open ports
    Add your own hosts outside the local network
    Integrated traceroute
    Integrated Metasploit framework RPCd
        Search hosts for known vulnerabilities via integrated Metasploit daemon
        Adjust exploit settings, launch, and create shell consoles on exploited systems
        More coming
    Forge TCP/UDP packets
    Perform man in the middle attacks (MITM) including:
        Image, text, and video replacement-- replace your own content on unencrypted web pages
        JavaScript injection-- add your own javascript to unencrypted web pages.
        password sniffing ( with common protocols dissection )
        Capture pcap network traffic files
        Real time traffic manipulation to replace images/text/inject into web pages
        DNS spoofing to redirect traffic to different domain
        Break existing connections
        Redirect traffic to another address
        Session Hijacking-- listen for unencrypted cookies and clone them to take Web session

 
